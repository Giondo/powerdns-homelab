# https://gitlab.com/Giondo/powerdns-homelab


<!-- This is commented out.

[![GitHub Stars](https://img.shields.io/github/stars/linuxserver/docker-duplicati.svg?color=94398d&labelColor=555555&logoColor=ffffff&style=for-the-badge&logo=github)](https://github.com/linuxserver/docker-duplicati)
[![GitHub Release](https://img.shields.io/github/release/linuxserver/docker-duplicati.svg?color=94398d&labelColor=555555&logoColor=ffffff&style=for-the-badge&logo=github)](https://github.com/linuxserver/docker-duplicati/releases)

[![GitLab Container Registry](https://img.shields.io/static/v1.svg?color=94398d&labelColor=555555&logoColor=ffffff&style=for-the-badge&label=GitLab&message=GitLab%20Registry&logo=gitlab)](https://gitlab.com/Giondo/powerdns-homelab/container_registry)
[![MicroBadger Layers](https://img.shields.io/microbadger/layers/giondo/cmsrails-refinery_app.svg?color=94398d&labelColor=555555&logoColor=ffffff&style=for-the-badge)](https://microbadger.com/images/giondo/cmsrails-refinery_app "Get your own version badge on microbadger.com")
[![Docker Pulls](https://img.shields.io/docker/pulls/giondo/cmsrails-refinery_app.svg?color=94398d&labelColor=555555&logoColor=ffffff&style=for-the-badge&label=pulls&logo=docker)](https://hub.docker.com/r/giondo/cmsrails-refinery_app)
[![Docker Stars](https://img.shields.io/docker/stars/giondo/cmsrails-refinery_app.svg?color=94398d&labelColor=555555&logoColor=ffffff&style=for-the-badge&label=stars&logo=docker)](https://hub.docker.com/r/giondo/cmsrails-refinery_app)
-->

## Usage

To download the image run

    docker pull registry.gitlab.com/giondo/powerdns-homelab

## Configuration

**Environment Configuration:**

* Generic settings

  * `PDNS_BACKEND=none` Choose between `mysql` and `sqlite3`. The default is `none`, so you can provide your own configuration by mounting pdns.conf into
  * `PDNS_AUTOCONFIG=true`
  * `PDNS_LOCAL_ADDRESS=(empty)` Local address to bind to. If this variable is not set, the default entry in the config file will be used.
  * `PDNS_LOCAL_PORT=(empty)` Local port to bind to. If this variable is not set, the default entry in the config file will be used.
  * `PDNS_MASTER=(empty)` Default value of `master` setting. See PowerDNS documentation.
  * `PDNS_SLAVE=(empty)` Default value of `slave` setting. See PowerDNS documentation.
  * `PDNS_ALLOW_AXFR_IPS=(empty)` Default value of `allow-axfr-ips` setting. See PowerDNS documentation.
  * `PDNS_ALLOW_DNSUPDATE_FROM=(empty)` Default value of `allow-dnsupdate-from` setting. See PowerDNS documentation.
  * `PDNS_ALLOW_NOTIFY_FROM=(empty)` Default value of `allow-notify-from` setting. See PowerDNS documentation.
  * `PDNS_ALLOW_UNSIGNED_NOTIFY=(empty)` Default value of `allow-unsigned-notify` setting. See PowerDNS documentation.
  * `PDNS_TRUSTED_NOTIFICATION_PROXY=(empty)` Default value of `trusted-notification-proxy` setting. See PowerDNS documentation.
  * `PDNS_DNSUPDATE=(empty)` Default value of `dnsupdate` setting. See PowerDNS documentation.
  * `PDNS_LOCAL_ADDRESS=0.0.0.0` if none set it will use 127.0.0.1
  * `PDNS_FORDWARD_ZONES_FILE_CONTENT=example.com=127.0.0.1:5053` if none set nothing will be created
  * `PDNS_FORDWARD_ZONES_FILE=/etc/powerdns/recursor.d/internal.zone` This needs to be set in order to refer from recursor to auth dns server, remember we are running both process here, recursor and authoritative dns server

* SQLite3 settings

  * `SQLITE3_PATH=/var/lib/powerdns/pdns.db` Path and filename of SQLite3 database

* MySQL connection settings

  * `MYSQL_HOST=localhost`
  * `MYSQL_PORT=3306`
  * `MYSQL_USER=dbuser`
  * `MYSQL_PASSWORD=<empty>` Password
  * `MYSQL_NAME=powerdns` Name of database

* Running as User

  * `PUID=$ID`
  * `PGID=$GID`


* API Key

  * `PDNS_API_KEY=none` By setting an API key, the built-in webserver and the HTTP API  will be activated. It runs on 0.0.0.0/0 on port 8081 in the container, so if you don't want to provide the API publically, just omit the port setting on 8081 in the docker run command or the compose file.

Skip modifying these parameters by setting PDNS_AUTOCONFIG to "false".

## Usage example

```
docker run -d \
  --name=pdns-server \
  -e PUID=1000 \
  -e PGID=1000 \
  -e PDNS_API_KEY={somerandomgkey} \
  -e TZ=Europe/Amsterdam \
  -e PDNS_BACKEND=sqlite3 \
  -p 53:53/udp \
  -p 8081:8081 \
  -p 53:53 \
  -v <path to data>:/var/lib/powerdns \
  -v <path to data>:/etc/powerdns \
  --restart unless-stopped \
  registry.gitlab.com/giondo/powerdns-homelab:latest
```

## Block list
By using a compiled host files from

https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts


export-etc-hosts=yes
etc-hosts-file=/app/StevenBlack.hosts
sed -i "s/0.0.0.0/127.0.0.1/g" hosts

## Credits

**
This is a Copy of https://github.com/geschke/docker-powerdns-server
just fixed the sqlite3 backend and added pdns recursor dns
**

** Pdns Admin
https://github.com/ngoduykhanh/PowerDNS-Admin
**


## TO-DO

* add pdns admin webserver

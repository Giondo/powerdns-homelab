# FROM ubuntu:focal
# FROM ghcr.io/linuxserver/baseimage-mono:LTS
FROM ghcr.io/linuxserver/baseimage-ubuntu:focal


LABEL maintainer="David Pasqua <giondog@gmail.com>"
LABEL last_changed="2020-12-28"
LABEL build_version="VirtualInfra.online version:- ${VERSION} Build-date:- ${BUILD_DATE}"


# necessary to set default timezone Etc/UTC
ENV DEBIAN_FRONTEND noninteractive

# Install all dependencies
RUN apt-get update \
  && apt-get -y upgrade \
  && apt-get -y dist-upgrade \
  && apt-get install -y ca-certificates \
  && apt-get install -y --no-install-recommends \
  && apt-get install -y locales \
  && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
  && apt-get install -y vim bind9-dnsutils curl sqlite3 git gnupg pdns-server pdns-recursor pdns-backend-sqlite3 pdns-backend-mysql mysql-client \
  && rm -rf /var/lib/apt/lists/*


EXPOSE 8081 53/udp 53/tcp
COPY root/ /
VOLUME /var/lib/powerdns/ /etc/powerdns/


# USER root
# RUN mkdir -p /app
# ADD app /app
# RUN chmod 755 /app/entrypoint.sh && chmod 755 /app/wait-for-it.sh
# RUN chown -R pdns:pdns /app

#USER pdns
# ENTRYPOINT ["/app/entrypoint.sh"]
# CMD ["app:start"]
